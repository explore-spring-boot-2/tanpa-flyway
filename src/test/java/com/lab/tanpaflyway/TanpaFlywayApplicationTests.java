package com.lab.tanpaflyway;

import com.lab.tanpaflyway.dao.VetDao;
import com.lab.tanpaflyway.entity.Vet;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class TanpaFlywayApplicationTests {

	@Test
	void contextLoads() {
	}

	@Autowired
	VetDao vetDao;

	@Test
	void readFromDatabase() {
		int id = 1;
		Vet vet = vetDao.findById(id)
				.orElseThrow(() -> new IllegalArgumentException("ID tidak ditemukan: " + id));
		System.out.println(">>> first name: " + vet.getFirstName());
		System.out.println(">>>  last name: " + vet.getLastName());
	}

}
