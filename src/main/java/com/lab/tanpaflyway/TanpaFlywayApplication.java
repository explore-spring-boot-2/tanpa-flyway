package com.lab.tanpaflyway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TanpaFlywayApplication {

	public static void main(String[] args) {
		SpringApplication.run(TanpaFlywayApplication.class, args);
	}

}
