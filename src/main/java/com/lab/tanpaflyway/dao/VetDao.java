package com.lab.tanpaflyway.dao;

import com.lab.tanpaflyway.entity.Vet;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VetDao extends JpaRepository<Vet, Integer> {
}
