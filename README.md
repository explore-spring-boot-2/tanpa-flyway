# Tanpa Flyway

Setup / Init database tidak harus menggunakan flywaydb

ref: 
- [Baeldung](https://www.baeldung.com/spring-boot-data-sql-and-schema-sql)
- [Pet Clinic](https://github.com/spring-projects/spring-petclinic/)

![Screenshot1](./readme/img/ss01.png "Screenshot 1")